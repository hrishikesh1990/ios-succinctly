//
//  ViewController.m
//  Audio
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.;
    NSURL *soundFileURL = [[NSBundle mainBundle]
                           URLForResource:@"good-morning-heartache" withExtension:@"mp3"];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    _player.delegate = self;
    
    _playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_playButton setTitle:@"Play" forState:UIControlStateNormal];
    [_playButton addTarget:self action:@selector(playOrPause:)
          forControlEvents:UIControlEventTouchUpInside];
    _playButton.frame = CGRectMake(100.0, 170.0, 120.0, 40.0);
    [[self view] addSubview:_playButton];
    
    _stopButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_stopButton setTitle:@"Stop" forState:UIControlStateNormal];
    [_stopButton addTarget:self
                    action:@selector(stop:)
          forControlEvents:UIControlEventTouchUpInside];
    _stopButton.frame = CGRectMake(100.0, 230.0, 120.0, 40.0);
    [[self view] addSubview:_stopButton];
}

- (void)playOrPause:(id)sender {
    if (_player.playing) {
        [_player pause];
        [_playButton setTitle:@"Play" forState:UIControlStateNormal]; } else {
            [_player play];
            [_playButton setTitle:@"Pause" forState:UIControlStateNormal]; }
}

- (void)stop:(id)sender {
    [_player stop];
    _player.currentTime = 0;
    [_playButton setTitle:@"Play" forState:UIControlStateNormal]; }

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag { [self stop:nil];
    NSLog(@"Song finished playing!");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
