//
//  ViewController.h
//  Audio
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController <AVAudioPlayerDelegate>
{
    AVAudioPlayer *_player;
    UIButton *_playButton;
    UIButton *_stopButton;
}



@end
