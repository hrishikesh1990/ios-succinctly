//
//  ViewController.m
//  Audio
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

@synthesize blip=_blip;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.;
    CFURLRef blipURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(),
                                               CFSTR ("blip"),
                                               CFSTR ("wav"),
                                               NULL);
    NSLog(@"%@", blipURL);
    AudioServicesCreateSystemSoundID(blipURL, &_blip);
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [aButton setTitle:@"Blip" forState:UIControlStateNormal];
    [aButton addTarget:self
                action:@selector(playBlip:)
      forControlEvents:UIControlEventTouchUpInside]; aButton.frame = CGRectMake(100.0, 200.0, 120.0, 40.0); [[self view] addSubview:aButton];
}

- (void)playBlip:(id)sender {
    AudioServicesPlaySystemSound(_blip);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
