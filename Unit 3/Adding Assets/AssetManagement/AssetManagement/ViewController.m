//
//  ViewController.m
//  AssetManagement
//
//  Created by Macbook Pro on 5/6/13.
//  Copyright (c) 2013 Ryan Hodson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Find the resource.
    NSString *imagePath = [[NSBundle mainBundle]
                           pathForResource:@"syncfusion-logo"
                           ofType:@"png"];
    // Load the resource.
    UIImage *imageData = [[UIImage alloc]
                          initWithContentsOfFile:imagePath];
    NSLog(@"Image path: %@", imagePath);
    if (imageData != nil) {
        UIImageView *imageView = [[UIImageView alloc]
                                  initWithImage:imageData];
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        imageView.contentMode = UIViewContentModeScaleAspectFit; CGRect frame = imageView.frame;
        frame.size.width = screenBounds.size.width; imageView.frame = frame;
        [[self view] addSubview:imageView];
        NSLog(@"Image size: %.0fx%.0f",imageData.size.width, imageData.size.height);
        NSLog(@"Could not load the file");
    } else { }
}
                                                                                             

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
