Notes on resources used in the book.
 
* The Ella Fitzgerald song used in one of the samples came from http://www.jazz-on-line.com.
* The author prepared the blip.wav file himself using http://thirdcog.eu/apps/cfxr.
* All of the graphics were created with Inkscape.
 
Errata
 
Page 79 (PDF document) - Encoding when reading back file contents should be set to the same as when writing (NSUnicodeStringEncoding). We will correct this in the book and publish an update.
 
We hope you enjoy the book!
 
The Syncfusion Team